import React from 'react'
import {render} from 'react-dom'

class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
            msg: 'Hello world !'
        };
    }

    render(){
        return (<div>{this.state.msg}</div>)
    }
}

render(<App />, document.getElementById('container'));